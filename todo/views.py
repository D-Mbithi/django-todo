from django.shortcuts import render, redirect
from .models import Todo
from .forms import TodoForm


def todo_list(request):
    todo = Todo.objects.all()
    template = 'index.html'
    context = {'todo_list': todo}

    return render(request, template, context)


def todo_detail(request, id):
    todo = Todo.objects.get(id=id)
    template = 'todos/detail.html'
    context = {'todo': todo}

    return render(request, template, context)


def todo_create(request):
    form = TodoForm(request.POST or None)

    if form.is_valid():
        form.save()

        return redirect('/')

    template = 'todos/create.html'

    context = {
        'form': form
    }

    return render(request, template, context)


def todo_update(request, id):
    todo = Todo.objects.get(id=id)

    template = 'todos/update.html'

    form = TodoForm(request.POST or None, instance=todo)

    if form.is_valid():
        form.save()

        return redirect('/')

    context = {'form': form}

    return render(request, template, context)


def todo_delete(request, id):
    todo = Todo.objects.get(id=id)
    todo.delete()

    return redirect('/')
